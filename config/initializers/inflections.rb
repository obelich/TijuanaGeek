# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.plural /^(ox)$/i, '\1en'
#   inflect.singular /^(ox)en/i, '\1'
#   inflect.irregular 'person', 'people'
#   inflect.uncountable %w( fish sheep )

    inflect.irregular 'articulo', 'articulos'
    inflect.irregular 'categoria', 'categorias'
    inflect.irregular 'noticia', 'noticias'
    inflect.irregular 'imagen', 'imagenes'
    inflect.irregular 'producto', 'productos'
    inflect.irregular 'galeria', 'galerias'
    inflect.irregular 'menu', 'menus'
    inflect.irregular 'tipo', 'tipos'
    inflect.irregular 'marca', 'marcas'
    inflect.irregular 'menu_tipo', 'menus_tipos'
    inflect.irregular 'categoria_menu', 'categorias_menus'
    inflect.irregular 'submenu', 'submenus'
    inflect.irregular 'subsubmenu', 'subsubmenus'
    inflect.irregular 'pedido', 'pedidos'
    inflect.irregular 'menu_producto', 'menus_productos'
    inflect.irregular 'cosa', 'cosas'
    inflect.irregular 'direccion', 'direcciones'
    inflect.irregular 'direccion', 'direcciones'

    # inflect.irregular 'menu_producto', 'menus_productos'
    # inflect.irregular 'categoria', 'categorias'
end

# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end
