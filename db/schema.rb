# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150214205343) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "categorias", force: :cascade do |t|
    t.string   "categoria"
    t.text     "descripcion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "categorias_menus", id: false, force: :cascade do |t|
    t.integer "menu_id"
    t.integer "categoria_id"
  end

  add_index "categorias_menus", ["categoria_id"], name: "index_categorias_menus_on_categoria_id", using: :btree
  add_index "categorias_menus", ["menu_id"], name: "index_categorias_menus_on_menu_id", using: :btree

  create_table "galerias", force: :cascade do |t|
    t.string   "imagen"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "producto_id"
  end

  create_table "marcas", force: :cascade do |t|
    t.string   "marca"
    t.text     "descripcion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "menus", force: :cascade do |t|
    t.string   "nombremenu"
    t.integer  "categoria_id"
    t.text     "descripcion"
    t.string   "imagenmenu"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "menu_id"
    t.string   "posicion"
    t.boolean  "status",       default: true
    t.boolean  "primario",     default: false
  end

  create_table "menus_productos", id: false, force: :cascade do |t|
    t.integer "menu_id"
    t.integer "producto_id"
  end

  add_index "menus_productos", ["menu_id"], name: "index_menus_productos_on_menu_id", using: :btree
  add_index "menus_productos", ["producto_id"], name: "index_menus_productos_on_producto_id", using: :btree

  create_table "menus_tipos", id: false, force: :cascade do |t|
    t.integer "menu_id"
    t.integer "tipo_id"
  end

  add_index "menus_tipos", ["menu_id"], name: "index_menus_tipos_on_menu_id", using: :btree
  add_index "menus_tipos", ["tipo_id"], name: "index_menus_tipos_on_tipo_id", using: :btree

  create_table "noticias", force: :cascade do |t|
    t.string   "titulo"
    t.text     "noticia"
    t.integer  "categoria_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "noticias", ["categoria_id"], name: "index_noticias_on_categoria_id", using: :btree

  create_table "pedido_productos", force: :cascade do |t|
    t.integer  "producto_id"
    t.integer  "pedido_id"
    t.integer  "cantidad"
    t.float    "costo"
    t.float    "totalproducto"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "pnombre"
  end

  add_index "pedido_productos", ["pedido_id"], name: "index_pedido_productos_on_pedido_id", using: :btree
  add_index "pedido_productos", ["producto_id"], name: "index_pedido_productos_on_producto_id", using: :btree

  create_table "pedidos", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "status"
    t.float    "total"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email"
    t.text     "comentario"
  end

  create_table "productos", force: :cascade do |t|
    t.string   "nombre"
    t.text     "descripcion"
    t.float    "costo"
    t.string   "imagen"
    t.integer  "categoria_id"
    t.boolean  "sleader"
    t.text     "sleadertext"
    t.float    "especial"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tpmoneda"
    t.boolean  "activo"
    t.text     "notas"
    t.boolean  "portada",             default: false
    t.string   "sku"
    t.boolean  "nuevo",               default: false
    t.float    "preciocosto"
    t.float    "preciosugerido"
    t.float    "porcentajeganacia"
    t.float    "costoproveedorenvio"
    t.string   "usarcosto"
    t.integer  "marca_id"
    t.integer  "tipo_id"
  end

  create_table "productos_submenus", id: false, force: :cascade do |t|
    t.integer "submenu_id"
    t.integer "producto_id"
  end

  add_index "productos_submenus", ["producto_id"], name: "index_productos_submenus_on_producto_id", using: :btree
  add_index "productos_submenus", ["submenu_id"], name: "index_productos_submenus_on_submenu_id", using: :btree

  create_table "productos_subsubmenus", id: false, force: :cascade do |t|
    t.integer "subsubmenu_id"
    t.integer "producto_id"
  end

  add_index "productos_subsubmenus", ["producto_id"], name: "index_productos_subsubmenus_on_producto_id", using: :btree
  add_index "productos_subsubmenus", ["subsubmenu_id"], name: "index_productos_subsubmenus_on_subsubmenu_id", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id", using: :btree
  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree

  create_table "submenus", force: :cascade do |t|
    t.string   "nombremenu"
    t.integer  "menu_id"
    t.text     "descripcion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "subsubmenus", force: :cascade do |t|
    t.string   "nombremenu"
    t.integer  "submenu_id"
    t.text     "descripcion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipos", force: :cascade do |t|
    t.string   "tipo"
    t.text     "descripcion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "dcinumber"
    t.string   "nombre"
    t.string   "apellidos"
    t.text     "direccion"
    t.date     "fechanacimiento"
    t.string   "ciudad"
    t.integer  "cp"
    t.string   "pais"
    t.string   "telefono"
    t.string   "estado"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "users_roles", id: false, force: :cascade do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id", using: :btree

end
