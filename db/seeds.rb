# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

roles = Role.create([ {name: "Administrador" },
                      {name:  "Cliente"}

                    ])


usuarios = User.create([{:id=> '1',email: "obelich@gmail.com", password: "oscarm2128", :encrypted_password=>'$2a$10$uj9ssWR/TkBxCPbvyzt9Vu8U7vMqzU8hoYv.1ECMmaB.jrhk8w3uC' },
                        {:id=> '2',email: "root@tijuanageek.com", password: "oscarm2128", :encrypted_password=>'$2a$10$uj9ssWR/TkBxCPbvyzt9Vu8U7vMqzU8hoYv.1ECMmaB.jrhk8w3uC' }
                       ])
usuario = User.find_by(email: "obelich@gmail.com")
usuario.remove_role :Cliente
usuario.add_role :Administrador

usuario2 = User.find_by(email: "root@tijuanageek.com")
usuario2.remove_role :Cliente
usuario2.add_role :Administrador

100.times{
productos = Producto.create([
  {nombre: "Fat Pack Khans of Tarkir", descripcion: '<p>Caja con 9 sobres de Khans of tarkir</p>', costo: 500, sleader: false, activo: true, portada: true, sku: '12345', nuevo: true, usarcosto: 'regular', marca_id: 1, tipo_id: 2, categoria_id: 1, porcentajeganacia: 15, tpmoneda: 'Pesos MX', imagen: nil   }
  ])
}

marcas = Marca.create([
  {id: 1, marca: 'Wizard of the coast', descripcion: '' },
  {id: 2, marca: 'Blizard', descripcion: '' },
  {id: 3, marca: 'Jinx', descripcion: '' },
  {id: 4, marca: 'Ultra Pro', descripcion: '' }
  ])

categorias = Categoria.create([
  {id: 1, categoria: 'Juegos de mesa', descripcion: '' },
  {id: 2, categoria: 'juguetes', descripcion: '' },
  {id: 3, categoria: 'Juegos de cartas', descripcion: '' }
  ])

  tipos = Tipo.create([
    {id: 1, tipo: 'Accesorios', descripcion: '' },
    {id: 2, tipo: 'ropa', descripcion: '' },
    {id: 3, tipo: 'varios', descripcion: '' }
    ])
