class AddColumnEmailToPedidos < ActiveRecord::Migration
  def change
    add_column :pedidos, :email, :string
    add_column :pedidos, :comentario, :text
  end
end
