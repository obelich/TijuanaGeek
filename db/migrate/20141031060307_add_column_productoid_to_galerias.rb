class AddColumnProductoidToGalerias < ActiveRecord::Migration
  def change
    remove_column :galerias, :articulo_id, :integer
    add_column :galerias, :producto_id, :integer
  end
end
