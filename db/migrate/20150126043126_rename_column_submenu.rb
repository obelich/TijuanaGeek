class RenameColumnSubmenu < ActiveRecord::Migration
  def change
    rename_column :subsubmenus, :submenu, :nombremenu
    rename_column :submenus, :submenu, :nombremenu
  end
end
