class SubsubmenusProductos < ActiveRecord::Migration
  def change
    create_table :productos_subsubmenus, :id => false do |t|
      t.references :subsubmenu
      t.references :producto
    end
    add_index :productos_subsubmenus, :subsubmenu_id
    add_index :productos_subsubmenus, :producto_id
  end
end
