class CreateMenus < ActiveRecord::Migration
  def change
    create_table :menus do |t|
      t.string :nombremenu
      t.integer :categoria_id
      t.text :descripcion
      t.string :imagenmenu

      t.timestamps
    end
  end
end
