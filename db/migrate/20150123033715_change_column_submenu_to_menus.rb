class ChangeColumnSubmenuToMenus < ActiveRecord::Migration
  def change
    rename_column :menus, :submenu_id, :menu_id
  end
end
