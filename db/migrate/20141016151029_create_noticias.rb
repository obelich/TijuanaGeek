class CreateNoticias < ActiveRecord::Migration
  def change
    create_table :noticias do |t|
      t.string :titulo
      t.text :noticia
      t.integer :categoria_id

      t.timestamps
    end
    add_index :noticias, :categoria_id
  end
end
