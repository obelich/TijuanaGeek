class CreateGaleria < ActiveRecord::Migration
  def change
    create_table :galerias do |t|
      t.string :imagen
      t.integer :articulo_id

      t.timestamps
    end
    add_index :galerias, :articulo_id
  end
end
