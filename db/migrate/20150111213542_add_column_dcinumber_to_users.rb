class AddColumnDcinumberToUsers < ActiveRecord::Migration
  def change
    add_column :users, :dcinumber, :string
    add_column :users, :nombre, :string
    add_column :users, :apellidos, :string
    add_column :users, :direccion, :text
    add_column :users, :fechanacimiento, :date

    add_column :productos, :notas, :text
    add_column :productos, :portada, :boolean, :default=>false
    add_column :productos, :sku, :string
    remove_column :productos, :nuevo, :boolean
    add_column :productos, :nuevo, :boolean, :default=>false
  end
end
