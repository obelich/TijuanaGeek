class Multiplesmuchos < ActiveRecord::Migration
  def change
    create_table :menus_productos, :id => false do |t|
      t.references :menu
      t.references :producto
    end
    add_index :menus_productos, :menu_id
    add_index :menus_productos, :producto_id
    
    create_table :productos_submenus, :id => false do |t|
      t.references :submenu
      t.references :producto
    end
    add_index :productos_submenus, :submenu_id
    add_index :productos_submenus, :producto_id

  end
end
