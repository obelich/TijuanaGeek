class PedidosProductos < ActiveRecord::Migration
  def change
    create_table :pedido_productos do |t|
      t.integer :producto_id
      t.integer :pedido_id
      t.integer :cantidad
      t.float :costo
      t.float :totalproducto
      t.timestamps
    end
    add_index :pedido_productos, :producto_id
    add_index :pedido_productos, :pedido_id
  end
end
