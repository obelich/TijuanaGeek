class AddColumnVariasToMenus < ActiveRecord::Migration
  def change
    add_column :menus, :submenu_id, :integer
    add_column :menus, :posicion, :string
    add_column :menus, :status, :boolean, :default=>true
    add_column :menus, :primario, :boolean, :default=>false
  end
end
