class CreateProductos < ActiveRecord::Migration
  def change
    create_table :productos do |t|
      t.string :nombre
      t.text :descripcion
      t.float :costo
      t.string :imagen
      t.integer :categoria_id
      t.boolean :nuevo
      t.boolean :sleader
      t.text :sleadertext
      t.float :especial

      t.timestamps
    end
  end
end
