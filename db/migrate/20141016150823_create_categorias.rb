class CreateCategorias < ActiveRecord::Migration
  def change
    create_table :categorias do |t|
      t.string :categoria
      t.text :descripcion

      t.timestamps
    end
  end
end
