class AddProductoscategorias < ActiveRecord::Migration
  def change
    create_table :categorias_menus, :id => false do |t|
      t.references :menu
      t.references :categoria
    end
    add_index :categorias_menus, :menu_id
    add_index :categorias_menus, :categoria_id
  end
end
