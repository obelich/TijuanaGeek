class AddColumnDatosadicionalesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :ciudad, :string
    add_column :users, :cp, :integer
    add_column :users, :pais, :string
    add_column :users, :telefono, :string
    add_column :users, :estado, :string
  end
end
