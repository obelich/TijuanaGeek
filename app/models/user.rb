class User < ActiveRecord::Base
  rolify
  after_create :set_default_role

  private
  def set_default_role
    usuario = User.find_by(id: id)
    usuario.add_role "Cliente"
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
