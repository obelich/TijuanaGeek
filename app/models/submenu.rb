class Submenu < ActiveRecord::Base
  belongs_to :menu
  has_many :subsubmenus

  has_and_belongs_to_many :productos
end
