class Producto < ActiveRecord::Base
  # has_and_belongs_to_many :menu
  has_and_belongs_to_many :menus, join_table: :menus_productos
  has_and_belongs_to_many :submenus
  has_and_belongs_to_many :subsubmenus
  # has_and_belongs_to_many :pedidos
  has_many :pedido_productos
  has_many :pedidos, through: :pedido_productos

  has_many :galerias
  belongs_to :categoria
  belongs_to :marca
  belongs_to :tipo
  mount_uploader :imagen, ImagenUploader


  # validate :nombre, :categoria_id, :costo, presence: true
end
