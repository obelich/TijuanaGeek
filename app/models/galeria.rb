class Galeria < ActiveRecord::Base
  belongs_to :producto

  mount_uploader :imagen, ImagenUploader

end
