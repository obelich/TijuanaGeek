json.array!(@productos) do |producto|
  json.extract! producto, :id, :nombre, :descripcion, :costo, :imagen, :categoria_id, :nuevo, :sleader, :sleadertext, :especial
  json.url producto_url(producto, format: :json)
end
