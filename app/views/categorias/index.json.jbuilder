json.array!(@categorias) do |categoria|
  json.extract! categoria, :id, :categoria, :descripcion
  json.url categoria_url(categoria, format: :json)
end
