json.array!(@galeria) do |galerium|
  json.extract! galerium, :id, :imagen, :articulo_id
  json.url galerium_url(galerium, format: :json)
end
