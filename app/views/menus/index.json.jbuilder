json.array!(@menus) do |menu|
  json.extract! menu, :id, :nombremenu, :categoria_id, :descripcion, :imagenmenu
  json.url menu_url(menu, format: :json)
end
