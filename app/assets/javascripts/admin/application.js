// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require turbolinks
//= require bootstrap
//= require tinymce-jquery
//= require angular
//= require angular-resource
//= require angular-local-storage
//= require ng-rails-csrf


(function() {
  var app =  angular.module('dashboard', ['LocalStorageModule', 'ngResource', 'ng-rails-csrf']);


  app.config(['localStorageServiceProvider', function(localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('ls');
  }]);

  app.controller('menu', ['$scope', '$http', function($scope, $http) {
    $scope.pedidos = 0;

    $scope.contpedidos = function(){
      $http.get('/api/v1/cosas/contarpedidos.json').
          success(function(data, status, headers, config) {

            $scope.pedidos = data;

          });
    }

  }]);





})();

