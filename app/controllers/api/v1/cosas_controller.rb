class Api::V1::CosasController < ApplicationController
  before_action :set_pedido, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:create]
  # load_and_authorize_resource except: [:create]

  def contarpedidos
    @pedidos = Pedido.where(status: 'Solicitud de pedido').count
    # raise "hola"
    respond_with(@pedidos)
  end

end