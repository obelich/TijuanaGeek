class SubsubmenusController < ApplicationController
  before_action :set_subsubmenu, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :validarusuario
  before_action :set_dropdownlist, only: [:new, :edit, :update, :create]
  layout 'admin'
  respond_to :html


  def validarusuario
    # raise StandardError, "#{current_user.roles.map(&:name).inspect}"
    if current_user.has_role? :Cliente
      authorize! :accion, User
    else

    end
  end

  def index
    @subsubmenus = Subsubmenu.all
    respond_with(@subsubmenus)
  end

  def show
    respond_with(@subsubmenu)
  end

  def new
    @subsubmenu = Subsubmenu.new
    respond_with(@subsubmenu)
  end

  def edit
  end

  def create
    @subsubmenu = Subsubmenu.new(subsubmenu_params)
    @subsubmenu.save
    respond_with(@subsubmenu)
  end

  def update
    @subsubmenu.update(subsubmenu_params)
    respond_with(@subsubmenu)
  end

  def destroy
    @subsubmenu.destroy
    respond_with(@subsubmenu)
  end

  private
    def set_dropdownlist
      @submenus = Submenu.all
    end

    def set_subsubmenu
      @subsubmenu = Subsubmenu.find(params[:id])
    end

    def subsubmenu_params
      params.require(:subsubmenu).permit(:nombremenu, :submenu_id, :descripcion)
    end
end
