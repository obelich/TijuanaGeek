class GaleriasController < ApplicationController
  before_action :set_galeria, only: [:show, :edit, :update, :destroy]
  respond_to :html, :json
  before_action :authenticate_user!
  layout 'admin'
  before_action :validarusuario


  def validarusuario
    if current_user.roles.map(&:name) == ["Cliente"]
      authorize! :accion, User
    else

    end

  end


  def index
    if params[:producto_id]
      @producto = Producto.find_by(id: params[:producto_id] )
      @galerias = Galeria.where(producto_id: params[:producto_id])

    else
      @galerias = Galeria.all
    end

    respond_with(@galerias)
  end

  def show
    respond_with(@galeria)
  end

  def new
    @galeria = Galeria.new
    respond_with(@galeria)
  end

  def edit
  end

  def create
    @galeria = Galeria.new(galeria_params)
    respond_to do |format|
      if @galeria.save
        format.html { redirect_to producto_galerias_path(producto_id: @galeria.producto_id), notice: 'Archivo was successfully created.' }
        format.json { render :show, status: :created, location: @galeria }
      else
        format.html { render :new }
        format.json { render json: @galeria.errors, status: :unprocessable_entity }
      end
    end
  end

  def update

    respond_to do |format|
      if @galeria.update(galeria_params)
        format.html { redirect_to producto_galerias_path(producto_id: @galeria.producto_id), notice: 'Archivo was successfully created.' }
        format.json { render :show, status: :created, location: @galeria }
      else
        format.html { render :new }
        format.json { render json: @galeria.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @galeria.destroy
    respond_to do |format|
      format.html { redirect_to producto_galerias_path(producto_id: params[:producto_id]), notice: 'Archivo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    def set_galeria
      @galeria = Galeria.find(params[:id])
    end

    def galeria_params
      params.require(:galeria).permit(:imagen, :producto_id)
    end
end
