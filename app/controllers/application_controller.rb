class ApplicationController < ActionController::Base
  respond_to :html, :json
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_singup_parameters, if: :devise_controller?
  # before_filter :configure_permitted_parameters, if: :devise_controller?
  # before_filter :configure_singup_parameters, if: :devise_controller?
  before_action :dropdown



  protected
  def configure_singup_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit( :email,  :password, :password_confirmation, :dcinumber, :nombre, :apellidos, :direccion, :fechanacimiento,
               :ciudad, :cp, :pais, :telefono, :estado,
                                                             users_roles_attributes: [:user_id, :role_id]) }


  end


  private
  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_path, :alert => "No esta autorizado para realizar esa accion"
  end

  def dropdown
    @menus = Menu.all
  end


end
