class SubmenusController < ApplicationController
  before_action :set_submenu, only: [:show, :edit, :update, :destroy]
  before_action :set_dropdownlist, only: [:create, :edit, :new, :update]
  before_action :authenticate_user!
  before_action :validarusuario
  layout 'admin'
  respond_to :html


  def validarusuario
    # raise StandardError, "#{current_user.roles.map(&:name).inspect}"
    if current_user.has_role? :Cliente
      authorize! :accion, User
    else

    end
  end

  respond_to :html

  def index
    @submenus = Submenu.all
    respond_with(@submenus)
  end

  def show
    respond_with(@submenu)
  end

  def new
    @submenu = Submenu.new
    respond_with(@submenu)
  end

  def edit
  end

  def create
    @submenu = Submenu.new(submenu_params)
    @submenu.save
    respond_with(@submenu)
  end

  def update
    @submenu.update(submenu_params)
    respond_with(@submenu)
  end

  def destroy
    @submenu.destroy
    respond_with(@submenu)
  end

  private

    def set_dropdownlist
      @menus = Menu.all

    end


    def set_submenu
      @submenu = Submenu.find(params[:id])
    end

    def submenu_params
      params.require(:submenu).permit(:nombremenu, :menu_id, :descripcion)
    end
end
