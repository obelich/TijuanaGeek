== Proyecto Tijuana Geek

Este proyecto es para el lugar donde jugamos Magic The Gathering mi esposa, amigos y compañeros de trabajo, en la ciudad de Tijuana Baja California,
el desarrollo se esta realizando en mis tiempos libres.

La pagina se esta desarrollando usando

* Ruby On Rails 4.1.5
* PostgreSQL 9.x
* Bootstrap 3.x

El demo se puede ver en Heroku
http://tijuanageek.herokuapp.com/
